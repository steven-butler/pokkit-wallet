from flask import Flask, jsonify, request
from flask_cors import CORS
import json

from longhorn import Longhorn
from longhorn.transactions import Coin

app = Flask(__name__)
CORS(app, resources={r"*": {"origins": "*"}})

longhorn = Longhorn()


@app.route("/history/<coin>/<address>/", defaults={"start": 1, "limit": 10})
@app.route("/history/<coin>/<address>/<start>/<limit>")
def history(coin, address, start, limit):
  gateway_id = longhorn.get_gateway(coin)
  history = longhorn.get_history(gateway_id, address, start, limit)
  return jsonify(history)

@app.route("/last_transaction/<coin>/<address>")
def last_transaction(coin, address):
  gateway_id = longhorn.get_gateway(coin)
  transaction = longhorn.get_last_transaction(gateway_id, address)
  return jsonify(transaction)

@app.route("/transaction_detail/<coin>/<transaction>/")
def transaction_detail(coin, transaction):
  gateway_id = longhorn.get_gateway(coin)
  detail = longhorn.get_transaction(gateway_id, transaction)
  return jsonify(detail)


@app.route("/balance/<coin>/<address>/")
def balance(coin, address):
  gateway_id = longhorn.get_gateway(coin)
  balance = longhorn.get_balance(gateway_id, address)
  return jsonify(balance)


@app.route("/stats/<coin>/")
def stats(coin):
  gateway_id = longhorn.get_gateway(coin)
  stats = longhorn.get_gateway_stats(gateway_id)
  return jsonify(stats)


@app.route("/transact/<coin>/", methods=["POST"])
def transact(coin):
  gateway_id = longhorn.get_gateway(coin)
  data = json.loads(request.data)

  coin = Coin(coin.replace("Testnet", ""), data["from"])
  transaction = coin.send(data["to"], data["amount"])

  response = longhorn.transact(gateway_id, transaction)
  return jsonify(response)


@app.route("/api/Send/<coin>/", methods=["POST"])
def test_send(coin):
  gateway_id = longhorn.get_gateway(coin)
  data = request.data

  return jsonify([])

