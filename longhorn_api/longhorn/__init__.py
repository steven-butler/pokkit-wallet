import json
import traceback

import requests


class Longhorn(object):
  headers = {"apiKey": "51652333-f697-4afb-a893-4fcd07280800"}

  def __init__(self):
    self.gateways = self.get_gateways()

  def __get_data(self, url):
    r = requests.get(url, headers=self.headers)

    try:
      json_data = json.loads(r.content)

      return json_data
    except Exception as ex:
      print(f"{r.content}{ex}\n{traceback.format_exc()}")
      return {}

  def __post_data(self, url, data):
    try:
      headers = self.headers.copy()
      headers["Content-type"] = "application/json"
      r = requests.post(url, data=data, headers=headers)

      json_data = json.loads(r.content)

      return json_data
    except Exception as ex:
      print(f"{ex}\n{traceback.format_exc()}")
      return {}

  def get_gateway(self, name):
    if self.gateways is None:
      gateways = self.get_gateways()
    else:
      gateways = self.gateways

    try:
      return gateways[name]
    except KeyError as err:
      print("Gateway not found")

  def get_gateways(self):
    req = "https://longhorn.bullpay.com/api/GetGateways"

    json_data = self.__get_data(req)
    return json_data

  def get_gateway_stats(self, gateway_id):
    url = f"https://longhorn.bullpay.com/api/GetStats/{gateway_id}"
    json_data = self.__get_data(url)

    return json_data

  def get_transaction(self, gateway_id, transaction_id):
    url = f"https://longhorn.bullpay.com/api/GetTransaction/{gateway_id}/{transaction_id}"
    json_data = self.__get_data(url)

    return json_data

  def get_last_transaction(self, gateway_id, address):
    return self.get_history(gateway_id, address, 1, 1)

  def get_history(self, gateway_id, address, start=1, count=100):
    url = f"https://longhorn.bullpay.com/api/GetTransactions/{gateway_id}/{address}/{start}/{count}"
    json_data = self.__get_data(url)

    transactions = []

    if "txs" in json_data.keys():
      for transaction_id in json_data["txs"]:
        try:
          transaction = self.get_transaction(gateway_id, transaction_id)
          amount = transaction["outputs"][0]["amount"]
          date = transaction["txdate"]

          if transaction["outputs"][0]["address"] == address:
            transaction_type = "receive"
          else:
            transaction_type = "send"

          url = f"https://longhorn.bullpay.com/?gateway={gateway_id}&data={transaction_id}"
          transactions.append({"amount": amount, "type": transaction_type, "url": url, "date": date})
        except Exception as ex:
          print(ex)

    return transactions

  def get_balance(self, gateway_id, address):
    url = f"https://longhorn.bullpay.com/api/GetBalance/{gateway_id}/{address}"
    json_data = self.__get_data(url)

    print(json_data)
    return json_data

  def transact(self, gateway_id, transaction):
    url = f"https://longhorn.bullpay.com/api/Send/{gateway_id}/"
    json_data = self.__post_data(url, transaction)
    print(json_data)
    return json_data


if __name__ == "__main__":
  longhorn = Longhorn()

  history = longhorn.get_history(longhorn.gateways["Dash"], "Xj5kZRxt5z8fF6ScKxE8hnJBRTX72exgkH")
  print(history)
