from cryptos import Dash, Bitcoin, BitcoinCash, Litecoin

SUPPORTED_TYPES = [
  "Bitcoin",
  "Litecoin",
  "Dash",
]

class Coin:
  def __init__(self, type, key, test=True):
    if type not in SUPPORTED_TYPES:
      raise TypeError("Unsupported coin")

    self.type = type
    self.key = key
    self.test = test

  def send(self, to, amount):
    connections = {
      "Bitcoin": Bitcoin,
      "Litecoin": Litecoin,
      "Dash": Dash
    }

    if self.test:
      connection = connections[self.type](testnet=True)
    else:
      connection = connections[self.type]

    # response = connection.send(self.key, to, amount)
    response = connection.preparesignedtx(self.key, to, amount)
    return response

